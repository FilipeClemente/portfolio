---
title: Estágio na Capensis, LDA
date: 2020/2/1
description: Desenvolvimento de software, no ambito de conclusão de curso
tag: Estágio
---

# Informações

| **Empresa**       | **Datas**                         |
|-------------------|-----------------------------------|
| **Capensis, LDA** | **2020/02/10** até **2020/06/01** |


# Resumo

###### Desenvolvimento de software.
###### Entre eles aplicação mobile para sistema de picagem de ponto para um programa pré-existente Alteração de template de projeto
###### Desenvolvimento de programa de interligação de programa de faturação para loja online


## Alguns Projetos desenvolvidos

| **Categoria** | **Tecnologias**                                                 | **Descrição**                                                                                                         |
|---------------|-----------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------|
| Gestão        | React Native, PHP                                               | Aplicação para picagem de ponto com integração na plataforma existente de gestão de funcionamento interno da empresa. |
| Gestão        | Bootstrap, CSS, PHP                                             | Manutenção da aplicação de gestão de empresa e alteração de template e correção de diversos erros.                    |
| Comercial     | NodeJS, ReactJS, PHP, MYSQL, SQL Server, Wordpress, Woocommerce | Sistema de integração de informações entre Sage 50C e sistema de eCommerce em Wordprees com WooComerce .              |
