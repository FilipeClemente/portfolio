---
title: Emprego na Capensis, LDA
date: 2021/06/01
description: Desenvolvimento de software,
tag: Emprego, Desenvolvimento, Suporte Técnico
---

# Informações

| **Empresa**       | **Datas**                         |
|-------------------|-----------------------------------|
| **Capensis, LDA** | **2021/02/10** até **Atualmente** |


# Resumo

###### Desenvolvimento de software e suporte técnico em diversas aplicações.
Realizei suporte técnico em aplicações de faturação e contabilidade (Sage 50c, Sage for accounts e zonesoft).
Desenvolvi diversas aplicações de varios generos desde gestão de vendas, sincronizador de informaçãoes do Sage 50c para outro projeto online e sistema de mailing.

## Alguns Projetos desenvolvidos

| **Categoria** | **Tecnologias**                                  | **Descrição**                                                                                                                                                                                           |
|---------------|--------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Gestão        | ReactJS, PHP, Laravel, SQL Server, MySQL, NodeJS | Sistema de gestão de vendedores com registo de rotas e integração de artigos, clientes e fornecedores vindo do Sage 50c.                                                                                |
| Gestão        | ReactJS, PHP, Laravel                            | Sistema de Gestão de gabinete veterinário                                                                                                                                                               |
| Marketing     | Php, MySql, Bootstrap, Css, Python               | Sistema de Mailing para envio de campanhas por email. Utilizando o próprio servidor SMTP do cliente sendo o email enviado pela conta do cliente e não em nome do cliente como muitas plataformas fazem. |
| Segurança     | NodeJs, Sequelize, ReactJS, MySQL, express, Bash | Gestor de credenciais com sistema de encriptação total com chave de desencriptação única.                                                                                                               |

